var YOUR_CLIENT_ID = '29902034753-ubf9oqfe9ku5geg533jjonfapklk8frj.apps.googleusercontent.com';
var YOUR_REDIRECT_URI = 'http://localhost/profile-page';
function setData(data) {
    let images=document.getElementsByClassName("yt-dp");
    let names=document.getElementsByClassName("yt-name");
    let handles=document.getElementsByClassName("yt-handle");
    let subs=document.getElementsByClassName("yt-subs");
    for (let image of images) {
        image.src = data['items'][0]['snippet']['thumbnails']['high']['url'];
    }
    for (let name of names) {
        name.innerHTML=data['items'][0]['snippet']['title']; 
    }
    for (let handle of handles) {
        handle.innerHTML=data['items'][0]['snippet']['customUrl']; 
    }
    for (let sub of subs) {
        sub.innerHTML=data['items'][0]['statistics']['subscriberCount'] + ' '+"Subscribers"; 
    }
}
function setLikedVideos(data){
  data=data.slice(0,5)
  data.forEach(element => {
    let videoList=document.querySelector(".fav-yt-vids")
    videoList.insertAdjacentHTML("afterbegin",`
    <div class=" m-2" style=" width: 320px">
    <iframe width="320" height="240"
    src="https://www.youtube.com/embed/${element.id}">
    </iframe> 
    </div>
    `)
  });
}
function setMyVideos(data){
  data=data.slice(0,5)
  data.forEach(element => {
    let videoList=document.querySelector(".my-yt-vids")
    videoList.insertAdjacentHTML("afterbegin",`
    <div class="m-2">
    <iframe width="320" height="240"
    src="https://www.youtube.com/embed/${element.id.videoId}">
    </iframe> 
    </div>
    `)
  });
}
function oauth2SignIn() {
  // Google's OAuth 2.0 endpoint for requesting an access token
  var oauth2Endpoint = 'https://accounts.google.com/o/oauth2/v2/auth';

  // Create element to open OAuth 2.0 endpoint in new window.
  var form = document.createElement('form');
  form.setAttribute('method', 'GET'); // Send as a GET request.
  form.setAttribute('action', oauth2Endpoint);

  // Parameters to pass to OAuth 2.0 endpoint.
  var params = {'client_id': YOUR_CLIENT_ID,
                'redirect_uri': YOUR_REDIRECT_URI,
                'scope': 'https://www.googleapis.com/auth/youtube.force-ssl',
                'state': 'try_sample_request',
                'include_granted_scopes': 'true',
                'response_type': 'token'};
                // Add form parameters as hidden input values.
  for (var p in params) {
    var input = document.createElement('input');
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', p);
    input.setAttribute('value', params[p]);
    form.appendChild(input);
  }
  // Add form to page and submit it to open the OAuth 2.0 endpoint.
  document.body.appendChild(form);
  form.submit();
} 
function trySampleRequest() {
      
  let params = JSON.parse(localStorage.getItem('oauth2-test-params'));
  if (params && params['access_token']) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET',
        'https://www.googleapis.com/youtube/v3/channels?part=snippet&mine=true&part=statistics&' +
        'access_token=' + params['access_token']);
    xhr.onreadystatechange = async function(e) {
      if (xhr.readyState === 4 && xhr.status === 200) {
        let data=JSON.parse(xhr.response)
        let myVids = await fetch('https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId='+data['items'][0]['id']+
        '&key='+ YOUR_CLIENT_ID+ 'HTTP/1.1',
        {'method':'get',
        'headers':{
            'Authorization': 'Bearer ' + params['access_token'],
            'Accept': 'application/json'
        }})
        myVids=await myVids.json();
        myVids=myVids.items
        let likedVids = await fetch('https://www.googleapis.com/youtube/v3/videos?myRating=like&part=snippet',
        {'method':'get',
        'headers':{
            'Authorization': 'Bearer ' + params['access_token'],
            'Accept': 'application/json'
        }})
        likedVids=await likedVids.json();
        likedVids=likedVids.items
        setMyVideos(myVids)
        setLikedVideos(likedVids)
        setData(data);
        console.log(myVids);
        
      } else if (xhr.readyState === 4 && xhr.status === 401) {
        // Token invalid, so prompt for user permission.
        oauth2SignIn();
      }
    };
    xhr.send(null);
  } else {
    oauth2SignIn();
  }
}
function mngytView(){
    $("#features4").hasClass("d-none")?$("#features4").removeClass("d-none"):'';
    !$("#features1-1").hasClass("d-none")?$("#features1-1").addClass("d-none"):'';
    !$("#features3").hasClass("d-none")?$("#features3").addClass("d-none"):'';     
    !$("#features2").hasClass("d-none")?$("#features2").addClass("d-none"):'';    
    !$("#features5").hasClass("d-none")?$("#features5").addClass("d-none"):'';   
    !$("#features6").hasClass("d-none")?$("#features6").addClass("d-none"):'';
}
var fragmentString = location.hash.substring(1);
    
    // Parse query string to see if page request is coming from OAuth 2.0 server.
    // var params = {};
    // var regex = /([^&=]+)=([^&]*)/g, m;
    // while (m = regex.exec(fragmentString)) {
    //   params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
    // }
    // if (Object.keys(params).length > 0) {
    //   localStorage.setItem('oauth2-test-params', JSON.stringify(params) );
    //   if (params['state'] && params['state'] == 'try_sample_request') {
    //     trySampleRequest();
    //     mngytView()
    //   }
    // } 

document.getElementById("fatch3").addEventListener("click",function ytapi(){
    var fragmentString = location.hash.substring(1);
    
    // Parse query string to see if page request is coming from OAuth 2.0 server.
    var params = {};
    var regex = /([^&=]+)=([^&]*)/g, m;
    while (m = regex.exec(fragmentString)) {
      params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
    }
    
    if (Object.keys(params).length > 0) {
      localStorage.setItem('oauth2-test-params', JSON.stringify(params) );
      if (params['state'] && params['state'] == 'try_sample_request') {
        console.log("a");
        trySampleRequest();
        mngytView()
      }
    }else{
      oauth2SignIn();
    }
    
    // If there's an access token, try an API request.
    // Otherwise, start OAuth 2.0 flow.
    
})