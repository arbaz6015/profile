const clientId = '05d82d86ae7845509d1222836a024f60';
const redirectUri = 'http://localhost/profile-page';
const urlParams = new URLSearchParams(window.location.search);
let usercode = urlParams.get('code');
function setprofile(data){
    document.getElementById("spotifyDP").src=data.images[1]['url'];
    document.getElementById("about-user").innerHTML=`${data.display_name} | Country : ${data.country} | Followers : ${data.followers.total.toLocaleString('en-US')}`;
}
function setartists(data,i){
        document.getElementById(`spotify-artist-${i}`).src=data.images[0]['url'];
        document.getElementById(`spotify-artist-name-${i}`).innerHTML=data.name;
        document.getElementById(`spotify-artist-info-${i}`).innerHTML=`
        ${data.genres[0]||''},${data.genres[1]||''} | ${data.followers.total.toLocaleString('en-US')} followers`;
}
 function setplaylist(data){
    document.getElementById(`spotify-playlist-name`).innerHTML=data.name;
    ;

    const element = document.getElementById('spotify-playlist');
    element.src=`https://embed.spotify.com/?uri=${data.uri}`;

    document.getElementById(`spotify-playlist-info`).innerHTML=`
        BY:${data.owner.display_name}
     `;
}
function setmyMusic(data){
    document.getElementById('spotify-mymusic').src=`https://embed.spotify.com/?uri=${data}`;
}
function generateRandomString(length) {
    let text = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}
async function generateCodeChallenge(codeVerifier) {
    function base64encode(string) {
      return btoa(String.fromCharCode.apply(null, new Uint8Array(string)))
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=+$/, '');
    }

    const encoder = new TextEncoder();
    const data = encoder.encode(codeVerifier);
    const digest = await window.crypto.subtle.digest('SHA-256', data);

    return base64encode(digest);
}
function Authorization(){
    let codeVerifier = generateRandomString(128);
    generateCodeChallenge(codeVerifier).then(codeChallenge => {
        let state = generateRandomString(16);
        let scope = 'user-read-private user-read-email user-library-read playlist-read-private user-follow-read user-top-read';

        localStorage.setItem('code_verifier', codeVerifier);

        let args = new URLSearchParams({
          response_type: 'code',
          client_id: clientId,
          scope: scope,
          redirect_uri: redirectUri,
          state: state,
          code_challenge_method: 'S256',
          code_challenge: codeChallenge
        });

        window.location = 'https://accounts.spotify.com/authorize?' + args;
      });
}

 function storeAccesstoken(code){

    let codeVerifier = localStorage.getItem('code_verifier');
    let body = new URLSearchParams({
        grant_type: 'authorization_code',
        scope :'user-read-private user-read-email user-library-read playlist-read-private user-follow-read user-top-read',
        code: code,
        redirect_uri: redirectUri,
        client_id: clientId,
        code_verifier: codeVerifier,
      });

    const response = fetch('https://accounts.spotify.com/api/token', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: body
    })
    .then(async response => {
        if (!response.ok) {
        throw new Error('HTTP status ' + response.status);
        }
        return response.json();
    })
    .then(data => {
        localStorage.setItem('access_token', data.access_token);
        getProfile();
    })
    .catch(error => {
        console.error('Error:', error);
    });

}
function mngspotifyview(){
        $("#features6").hasClass("d-none")?$("#features6").removeClass("d-none"):'';
        !$("#features7-7").hasClass("d-none")?$("#features7-7").addClass("d-none"):'';
        !$("#features3").hasClass("d-none")?$("#features3").addClass("d-none"):'';
        !$("#features2").hasClass("d-none")?$("#features2").addClass("d-none"):'';
        !$("#features4").hasClass("d-none")?$("#features1-1").addClass("d-none"):'';
        !$("#features5").hasClass("d-none")?$("#features5").addClass("d-none"):'';
}
async function getProfile() {
    let accessToken = localStorage.getItem('access_token');
    const userResponse = await fetch('https://api.spotify.com/v1/me', {
      headers: {
        Authorization: 'Bearer ' + accessToken
      }
    });
    const artistResponse = await fetch('https://api.spotify.com/v1/me/top/artists?offset=0&limit=2', {
      headers: {
        Authorization: 'Bearer ' + accessToken
      }
    });
    const playlistResponse = await fetch('https://api.spotify.com/v1/me/playlists?offset=0&limit=1', {
      headers: {
        Authorization: 'Bearer ' + accessToken
      }
    });
    const myMusicResponse = await fetch('https://api.spotify.com/v1/me/tracks?offset=0&limit=1', {
      headers: {
        Authorization: 'Bearer ' + accessToken
      }
    });
    if(userResponse.status===401){
        localStorage.removeItem('access_token');
        localStorage.removeItem('code_verifier');
        Authorization();
    }else{
    const userdata = await userResponse.json();
    const artistData = await artistResponse.json();
    const playlistData = await playlistResponse.json();
    const myMusic =await myMusicResponse.json();
    artistData.items.forEach(async (element,i) => {
        i=i+1;
        let data = await fetch(`https://api.spotify.com/v1/artists/${element.id}`, {
      headers: {
        Authorization: 'Bearer ' + accessToken
      }});
      data=await data.json();
     setartists(data,i);
    });
    setprofile(userdata);
    setplaylist(playlistData.items[0]);
    setmyMusic(myMusic.items[0].track.uri)
    mngspotifyview()
    }

  }
if(usercode && !localStorage.getItem('access_token')){
    storeAccesstoken(usercode);
}
document.getElementById('fatch5').addEventListener('click', function() {

    if( localStorage.getItem('access_token')){
        getProfile();
    }
    else{
        Authorization();
    }
})
